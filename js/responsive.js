// Boutons :
let btnList = document.getElementById('btn-sidenav');
let spanClose = document.getElementById('close-list');

// List des taches accomplies :
let doneTodoList = document.getElementById('doneTodos');

// Ouverture :
btnList.addEventListener('click', () => {
    doneTodoList.style.display = 'block';
    btnList.style.display = 'none';
})

// Fermeture :
spanClose.addEventListener('click', () => {
    doneTodoList.style.display = 'none';
    btnList.style.display = 'block';
})



