"use strict";

let todosContainer = null;
let tempTodos = [];
let doneTodos = [];
let todoList = [];

// Recuperer l'element Html :
let doneList = document.getElementById('done-list');


// Charger les todos au chargements :
window.onload = refresh();
window.onload = OldList();


// Afficher la liste des taches :
function refresh() {

    if (todosContainer !== null) {
        todosContainer.innerHTML = '';
    }

    if (localStorage.getItem('todoList')) {

        // Récuperer la liste des taches :
        let todoList = JSON.parse(localStorage.getItem('todoList'));

        // Parcourir les todos :
        todoList.forEach(e => {

            // Donner une valeur a todosContainer :
            todosContainer = document.getElementById('todo-module');

            // Créer un model d'element pour lister les 'todos' :
            let todo = document.createElement('div')
            todo.className = 'todo';
            todosContainer.appendChild(todo);
            todo.innerHTML =
                `<div>
                <h3>
                    <ul>
                        <li class="title">
                            ${e.title}
                        </li>
                    </ul>
                </h3>
                <h5 class="date">   
                    <em>${e.date}</em>
                </h5>
            </div>
            <div>
                <h3>                
                    <button id="btn-done">
                        ${e.done}
                    </button>
                </h3>
            </div>`
                ;
        });


        // Passer une tache a 'Done' :
        let checkBtn = document.querySelectorAll('#btn-done');
        for (let i = 0; i < checkBtn.length; i++) {
            checkBtn[i].innerHTML = '<i class="far fa-check-circle"></i> Done ?';
            checkBtn[i].addEventListener('click', (e) => {

                // Mettre la valeur a True :
                todoList[i].done = true;

                // Enlever l'element du tableau todos et l'ajouter aux doneTodos :
                tempTodos = todoList.splice(i, 1);
                doneTodos.push(tempTodos[0]);
                // console.log('doneTodos ; ', doneTodos);

                // Sauver les valeurs en local :
                localStorage.setItem('doneTodos', JSON.stringify(doneTodos));

                // Si la todoList a des elements, on la sauvegarde aussi :  
                if (todoList.length > 0) {
                    localStorage.setItem('todoList', JSON.stringify(todoList));
                }
                else localStorage.removeItem('todoList');

                // console.log('todoList : ', todoList);

                // Rafraichier l'affichage :
                refresh();
                OldList();
            })
        }

    }
    else {
        todoList = [];
    }
    // console.log('todoList : ', todoList);

}


// Afficher la liste des taches accomplies :
function OldList() {

    // Remettre a zero l'affichage si donnéées presentent dans l'html :
    if (doneList.innerHTML !== '') {
        doneList.innerHTML = '';
    }

    // Si il y a des données dans an local nommées 'doneTodos' ;
    if (localStorage.getItem('doneTodos')) {

        // Recuperer les taches effectué enregistées en local :
        let oldTodo = JSON.parse(localStorage.getItem('doneTodos'));
        // console.log('oldTodo : ', oldTodo);

        // Afficher le titre de la tache dans un nouvel element 'li' :
        oldTodo.forEach(e => {
            let List = document.createElement('ul')
            List.className = 'list';
            doneList.appendChild(List);
            List.innerHTML =
                `<li>
                ${e.title}
            </li>`
                ;
        });
    }
    else {
        doneList.innerHTML = '<em>Aucune tâches accomplies !</em>';
    }
}


// Ajouter une tache :
const createTodo = (e) => {
    e.preventDefault();

    // Récuperer la value du form :
    let title = escapeHtml(document.getElementById('title').value);

    // Si elle est vide, signaler l'erreur :
    if (title == "") {
        alert("You'll have to give a task name !");
        return false;
    }
    else {
        // Créer un nouvel objet tache :
        let result = {
            title: title,
            date: new Date().toUTCString(),
            done: false
        }

        // Ajouter l'objet au tableau (liste) :
        todoList.push(result);
        // Remise a zero du formulaire :
        document.forms[0].reset();

        // Enregistrement en local de la nouvelle liste ;
        localStorage.setItem('todoList', JSON.stringify(todoList));

        // Rafraichir l'affichage de la liste :
        refresh();
    }
}


// Ecouter le click lors de l'envoi du formulaire, et ne pas recharger la page :
document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('addTodo').addEventListener('click', createTodo);
})


// Effacer le cache (localStorage) :
document.getElementById('reset-storage').addEventListener('click', () => {
    let x = confirm('Reset and remove the cache ?')
    if (x === true) {
        localStorage.clear();
        window.location.reload();
    }
    else {
        return false;
    }
})



/**
 *  Si la todo-list a plus d'un certain nombre d'element,
 *  alors evenement ? 
 *  -> afficher des images differentes.. 
 *  -> sonorité ? un musique avec volume qui augmente
 * 
 *  SON :
 *  Dans tout les cas mettre choix de sonorité (riviere, vague, oiseaux, foret, lagon ... new-york ?)
 */


/**
 * Securiser l'input
 * @param {string} text 
 */
function escapeHtml(text) {
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function (m) { return map[m]; });
}

