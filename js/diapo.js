// DIAPORAMA BACKGROUND-IMAGES 

let backImage = new Array(
    'url(../img/alvaro-reyes-cYhN8SErvFY-unsplash.jpg)',
    'url(../img/alana-harris-E1liYFaV62w-unsplash.jpg)',
    'url(../img/alexander-schimmeck--5damMHPgBM-unsplash.jpg)',
    'url(../img/franz-nawrath-1he0so8qL4k-unsplash.jpg)',
    'url(../img/gabriella-trejoss-rVBNJMZoXgY-unsplash.jpg)',
);

/**
 * @param {number} time 
 */
function diaporama(time) {
    let count = 0;
    setInterval(function () {
        document.body.style.backgroundImage = backImage[count];
        count++
        if (backImage.length <= count) {
            count = 0;
        }
    }, time);
}

document.body.style.backgroundImage = 'url(../img/alexander-schimmeck--5damMHPgBM-unsplash.jpg)';
diaporama(5000);