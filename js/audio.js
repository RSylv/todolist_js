let playerContainer = document.getElementById('player');
let audioPlayer = document.createElement('audio');
let songList = document.createElement('div');
let btnShowList = document.createElement('button');
let btnShowPlayer = document.getElementById('player-show');
let hidePlayer = document.getElementById('close-player');
let btnInfo = document.getElementById('player-show-info');

let songSrc = [
    "./media/01.mp3",
    // Tardes_Do_Brazil_Em_SunSeaBarIbiza(MixedByJordiTorres).mp3
    "./media/02.mp3",    
    // New_York_Jazz_Lounge_BarJazzClassics.mp3
    "./media/03.mp3",
    // Ella_Fitzgerald_And_Louis_Armstrong_EllaAndLouisFullAlbum.mp3
    "./media/04.mp3",
    // Bob_Marley_Uprising_(Full_remastered_2001).mp3
    "./media/05.mp3",
    // Astrix_Universo_Paralello_2019_2020_[FullSet].mp3
    "./media/06.mp3",
    // Funky_Groove_Jackin_House_ByCole2018(FunkWeekender).mp3
    "./media/07.mp3",
    // Panda_Dub_Horizons_2019_[FullAlbumAlbumComplet].mp3
    "./media/08.mp3",
    // TheBestTarantinoMovieTracks.mp3
    "./media/09.mp3",
    // GRANATA_FREE_BEER_[full album].mp3
    "./media/10.mp3",
    // Best_of_Electro_Swing_Mix_January_2017.mp3
    "./media/11.mp3",
    // A.N.I.M.A.L - Fin de un mundo enfermo (1994) FULL ALBUM
];


/**
 * @param {string} song 
 */
function changeSong(song) {
    if (!song) {
        audioPlayer.src = "./media/01.mp3";
    }
    else { audioPlayer.src = song; }

    audioPlayer.volume = 0.5;
    audioPlayer.play();
}

// Lecteur audio :
audioPlayer.controls = true;
audioPlayer.innerHTML = "Your browser does not support the <code>audio</code> element";
playerContainer.appendChild(audioPlayer);

// Btn show list :
btnShowList.className = "btn-show-hide-list";
btnShowList.innerHTML = '↓ <em>Cliquer ici pour afficher la liste</em> ↓';
playerContainer.appendChild(btnShowList);

// Song list div :
songList.className = "song-list hidden";
songList.innerHTML = '<button class="btn-song-choice">Tardes Do Brazil Em SunSeaBar Ibiza</button>';
songList.innerHTML += '<button class="btn-song-choice">New York Jazz Lounge</button>';
songList.innerHTML += '<button class="btn-song-choice">Ella Fitzgerald And Louis Armstrong</button>';
songList.innerHTML += '<button class="btn-song-choice">Bob Marley - Uprising</button>';
songList.innerHTML += '<button class="btn-song-choice">Astrix Universo Paralello</button>';
songList.innerHTML += '<button class="btn-song-choice">Funky Groove Jackin\' House</button>';
songList.innerHTML += '<button class="btn-song-choice">Panda Dub - Horizons</button>';
songList.innerHTML += '<button class="btn-song-choice">The Best Tarantino Movie Tracks</button>';
songList.innerHTML += '<button class="btn-song-choice">Granata - hip-hop</button>';
songList.innerHTML += '<button class="btn-song-choice">Electro Swing Mix</button>';
songList.innerHTML += '<button class="btn-song-choice">A.N.I.M.A.L. Fin de un mundo inferno</button>';

playerContainer.appendChild(songList);


let songChoice = document.querySelectorAll('.btn-song-choice');

for (let i = 0; i < songChoice.length; i++) {
    songChoice[i].addEventListener('click', () => {
        changeSong(songSrc[i]);
    })
}


// Lancer la lecture :
// changeSong();


// Afficher la liste des morceaux :
btnShowList.addEventListener('click', () => {
    songList.classList.toggle('hidden');

    // Changer le message du bouton :
    if (!songList.classList.contains('hidden')) {
        btnShowList.innerHTML = '↑ <em>Cacher la liste ? cliquer ici !</em> ↑';
    }
    else btnShowList.innerHTML = '↓ <em>Cliquer ici pour afficher la liste</em> ↓';
});




// Cacher le lecteur Audio :
hidePlayer.addEventListener('click', () => {
    btnShowPlayer.style.display = 'block';
    btnInfo.style.display = 'block';
    playerContainer.style.display = 'none';
})

// Afficher le lecteur :
btnShowPlayer.addEventListener('click', () => {
    btnShowPlayer.style.display = 'none';
    btnInfo.style.display = 'none';
    playerContainer.style.display = 'block';
})